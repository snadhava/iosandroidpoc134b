var rootRef = new Firebase('https://glowing-fire-6794.firebaseio.com');

function onClickSignUp() {
  var signUpText = document.getElementById("signInMessage");
  signUpText.style.display = "block";
}

// Form submission for logging in
function submitLoginForm() {
	var form = document.forms['loginForm'];
    var userObj = {
    	email: form['email'].value,
    	password: form['password'].value
    };

    authWithPassword(userObj);
    return false;
}

function authWithPassword(userObj) {
    console.log(userObj);
    rootRef.authWithPassword(userObj, function onAuth(error, user) {
		if (error) {
			console.log("Login Failed!", error);
            
            // display error message on screen
            $("#error-msg").show();
                        
		} else {
			console.log("Authenticated successfully with payload:", user);
            
            // transition to next page
            
            $("#error-msg").hide();
            console.log($("form.loginForm"))
            
            $("form.loginForm").addClass("animated slideOutDown")
            
            $("form.loginForm").bind("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function(){ 
                window.location.href = 'welcome.html';
            });
		}    	
    });
}