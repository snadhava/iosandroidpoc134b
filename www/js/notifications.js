var defaultSettingsObj = {
		notifications: true,
		sleepMode: false,
		hours: 21,
		mins: 0
	};

$(document).ready(function(){
	Notification.requestPermission();

	//apply default settings if setting profile does not exist
	FB.settingsRef.once("value", function(data) {
		if (data.val() == null) {
			FB.settingsRef.set(defaultSettingsObj);
		}
	})
	
	function checkDeadlines() {
		
		//get current time/day
		var now = new Date();
		var minuteCheck = now.getMinutes();
		var hourCheck = now.getHours();
		var dayCheck = now.getDay();
		
		//parses day of week into string
		switch(dayCheck) {
        case 0:
          var dayStr = "Sunday";
          break;
        case 1:
            var dayStr = "Monday";
          break;
        case 2:
            var dayStr = "Tuesday";
          break;
        case 3:
            var dayStr = "Wednesday";
          break;
        case 4:
            var dayStr = "Thursday";
          break;
        case 5:
            var dayStr = "Friday";
          break;
        case 6:
            var dayStr = "Saturday";
          break;
      }
		
		// for each habit, check when to be notified
		FB.habitsRef.once("value", function(data) {
			var habits = data.val();
			for (var i in habits) {
				var cli = habits[i];   
				// checkDeadlines runs every minute, which means we dont need additional checks based on time
				if(cli.times_up_today < cli.times && +(data.val().hours) == hourCheck && +(data.val().minutes) == minuteCheck && cli.dates[dayStr] && !sleepMode) {
					createNotification(cli);
				}
			}
		});
	}

	function createNotification(cli) {
		
		if (!"Notification" in window) {
			console.log("This browser does not support notifications.");
		}
		else if (Notification.permission === "granted") {
			// create notification
			var img = cli.icon_src;
			var text = 'Don\'t forget "' + cli.title + '" today!';
			var notification = new Notification('Virtue/Vice', {
				body: text,
				icon: img
			});
			window.navigator.vibrate(500);
		}
		else if (Notification.permission !== 'denied') {
			Notification.requestPermission(function(permission) {
				
				if (!('permission' in Notification)) {
					Notification.permission = permission;
				}
				// create notification
				if (permission === "granted") {
					var img = cli.icon_src;
					var text = 'Don\'t forget "' + cli.title + '" today!';
					var notification = new Notification('Virtue/Vice', {
						body: text,
						icon: img
					});
					window.navigator.vibrate(500);
				}
			});
		}
	}

	// using a setInterval to run the checkDeadlines() function every minute
	setInterval(checkDeadlines, 60000);
})

function applySettings() {
	var form = document.forms['notifications'];
	var timeArr = form.elements["hrs"].split(':');
	var settingsObj = {
			notifications: myonoffswitch1.checked,
			sleepMode: false,
			hours: timeArr[0],
			mins: timeArr[1]
		};

    FB.settingsRef.update(settingsObj, function onComplete(error) {
		if (error) {
            $("#edit-setting-error").html("Failed to save settings.");
			console.log('Synchronization failed');
		} else {
			console.log("Synchronization succeeded.");
			window.location.href = './list.html';
		}    	
    });
	return false;
}