var rootRef = new Firebase('https://glowing-fire-6794.firebaseio.com');

var submitRegisterForm = function() {
	var form = document.forms['registerForm'];
	var userObj = {
	    email: form['email'].value,
	    password: form['password'].value
	};

    createUser(userObj);
	return false;
};

function createUser(userObj) {
    rootRef.createUser(userObj, function (error, userData) {
		if (error) {
	    	console.log("Error creating user:", error);
	  	} else {
	    	console.log("Successfully created user account with uid:", userData.uid);
	    	window.location.href = './login.html';
	  	}
    });
}