$(document).ready(function(){
    // Set the date
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    $('#date-div').prepend(days[(new Date()).getDay()] + " ");

    var habit_list = $('#habit-list');
    var others_days_habit_list = $("#other-days-habits");
    
    var loaded = false;

    FB.habitsRef.on("value", function(data) {
        var bodyStr = '';
        var incompleted = '';
        var completed = '';
        var failed = '';
        var not_today = '';
        habit_list.html(bodyStr); 
        others_days_habit_list.html(bodyStr);

        var habits = data.val();
        for (var i in habits) {
            var cli = habits[i];
            var status = FB.checkStatus(habits[i]);
            console.log (cli.title + ' ' + status);
            meterVal = parseFloat(cli.times_up_today) / parseFloat(cli.times);   
            
            bodyStr = 
                '<li class="habit-item">' +
                    '<div class="habit-info-top">' +
                        '<span class="habit-header">' + cli.title + '</span>' +
                        '<span class="habit-delete"><i class="fa fa-times fa-lg" onclick="return showDeleteDialog(this, ' + "'" + i + "'" + ');""></i></span>' +
                    '</div>' +
                    '<div class="habit-confirm-delete">' +
                        '<span class="habit-confirm-delete-span">' +
                        'Are you sure you want to delete this habit? ' +
                        '<a href="#" onclick="return deleteHabit(this, ' + "'" + i + "'" + ');"">Yes</a> ' +
                        '<a href="#" onclick="return hideDeleteDialog(this);">No</a>' +
                        '</span>' +
                    '</div>' +
                    '<div class="habit-info-middle">' +
                        '<div class="habit-info-left">' +
                            //'<img class="habit-icon" src="../img/' + cli.icon + '" alt="habit icon">' +
                            '<img class="habit-icon" src="' + cli.icon_src + '" alt="habit icon">' +

                        '</div>' +
                        '<div class="habit-info-right">' +
                            '<p class="currStreak"><strong>' + cli.streak + '</strong> day(s) in a row! Best Record: <strong>' + cli.best_record + '</strong></p>';

            if (status != 0)
                bodyStr += 
                '<p class="message-total">' + 
                                '<meter class="habit-meter" min="0" max="1" low=".3" high="0.7" optimum="1.0" value="' + meterVal + '"></meter>' +
                '</p>' +
                '<p class="message-today">Completed: <strong>' + cli.times_up_today + '/' + cli.times + '</strong></p>';
            bodyStr +=
                        '</div>' +
                    '</div>' +
                    '<div class="habit-op">';
            if (status == 3) {
                bodyStr +=
                    '<button type="button" class="op op-thumb-up" onclick="thumbsUp(' + "'" + i + "'" + ');" title="done">' +
                        '<i class="fa fa-thumbs-up fa-2x"></i>' +
                    '</button>' +
                    '<button type="button" class="op op-thumb-down" onclick="thumbsDown(' + "'" + i + "'" + ');" title="done">' +
                        '<i class="fa fa-thumbs-down fa-2x"></i>' +
                    '</button>';
            } else if (status == 1) {
                bodyStr +=
                    '<button type="button" class="face smile">' +
                         '<i class="fa fa-smile-o fa-4x"></i>' +
                    '</button>';                    
            } else if (status == 2) {
                bodyStr +=
                    '<button type="button" class="face frown">' +
                         '<i class="fa fa-frown-o fa-4x"></i>' +
                    '</button>';                 
            }

            bodyStr +=
                    '<button type="button" class="op op-edit" onclick="location.href=\'edit.html?hid=' + i + '\'" title="edit habit">' +
                            '<i class="fa fa-pencil fa-2x"></i>' +
                        '</button>' +                        
                    '</div>' +
                '</li>'; 

            /*if (status == 3)
                incompleted += bodyStr;
            else if (status == 2)
                failed += bodyStr;
            else if (status == 1)
                completed += bodyStr;*/
            if (status == 0)
                not_today += bodyStr;
            else
                incompleted += bodyStr;
        }

        bodyStr = incompleted + completed + failed;

        if (!loaded) {
            habit_list.hide().html(bodyStr).fadeIn(); 
            others_days_habit_list.hide().html(not_today).fadeIn();
            $("#spinner").hide();
        }
        else {
            habit_list.html(bodyStr);      
            others_days_habit_list.html(not_today);
        }

        loaded = true;
    });
});

function thumbsUp(id) {
    FB.habitThumbsUp(id);
}

function thumbsDown(id) {
    FB.habitThumbsDown(id);
}

function deleteHabit(element, id){
    $(element).parents('div').parents('.habit-item').fadeOut("slow", function() {
        FB.habitDelete(id);
    });
    return false;
};

function showDeleteDialog(element, id){
    $(element).parents('div').next('.habit-confirm-delete').fadeIn();
    return false;
}

function hideDeleteDialog(element) {
    $(element).parents('div').fadeOut("slow");
    return false;
}

function showMsg(element){
    var msgElement = (element.parentNode.parentNode.getElementsByClassName("message"))[0];
    msgElement.style.visibility="visible";
}; 

function logout() {
    FB.logout();
};
/*function incrementStreak(element) {
    var el = (element.getElementsByClassName("currStreak"))[0];
    var elIntVal = parseInt(el.innerHTML);
    elIntVal++;
    (element.getElementsByClassName("currStreak"))[0].textContent = elIntVal.toString();
};

$(function(){
    var habitList = localStorage.getItem("habitList"); //Retrieve the stored data
    habitList = JSON.parse(habitList); //Converts string to object
    
    if(habitList == null) //If there is no data, initialize an empty array
		habitList = [];
    
    function List() {
        console.log('hello');
        $("#habit-list").html("");
        for(var i in habitList){
			var cli = JSON.parse(habitList[i]);
            var meterVal;
            if(cli.BestRecord == "0") {
                meterVal = 1.0;
            } else {
                meterVal = parseFloat(cli.Streak) / parseFloat(cli.BestRecord);
            }
            //console.log(cli.Icon);
            var bodyStr = '<li>' +
                            '<ul class="habit-info">' +
                                '<li><div class="habit-name">' + cli.Title + '</div></li>' +
                                '<li><img class="habit-icon" src="' + cli.Icon + '" alt="habit icon"></li>' +
                            '</ul>' +
                        '<div class="message">' +
                            '<span class="message-total">' + 
                                '<strong><span class="currStreak">' + cli.Streak + '</span></strong> day(s) in a row! Best Record: <strong>' + cli.BestRecord + '</strong><br>' +
                                '<meter min="0" max="1" low=".3" high="0.7" optimum="1.0" value="' + meterVal + '"></meter>' +
                            '</span><br>' +
                            '<span class="message-today">Completed <strong>1/1</strong> for today!</span>' +
                        '</div>' +
                        '<div class="habit-op">' +
                            '<button type="button" class="op op-done" onclick="showMsg(this);" title="done">' +
                                '<img src="../img/done.svg" alt="Done">' +
                            '</button>' +
                            '<button type="button" class="op op-edit" title="edit habit">' +
                                '<img src="../img/edit.svg" alt="Edit">' +
                            '</button>' + 
                            '<button type="button" class="op op-del" onclick="deleteHabit(this);" title="delete habit">' +
                                '<img src="../img/delete.svg" alt="Del">' +
                            '</button>' +
                        '</div>' +
                    '</li>'
                ;
            $("#habit-list").append(bodyStr);
        }
        
    };
    
    function Delete(){
		habitList.splice(selected_index, 1);
		localStorage.setItem("habitList", JSON.stringify(habitList));
		alert("habit deleted.");
	};
    
    List();
    
    $(".op-del").bind("click", function(){
		selected_index = parseInt($(this).attr("alt").replace("Del", ""));
		Delete();
		List();
	});
    
    $(".op-edit").bind("click", function() {
       window.location.href = './edit.html?' + 'editName=';
    });
    
    
});*/
