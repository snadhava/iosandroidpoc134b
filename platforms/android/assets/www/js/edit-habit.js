var habitRef;
var habit;
var reader;
var customImgChanged = false;
var oldFreq;

$(document).ready(function() {
	var params = getSearchParameters();

	if (!params['hid']) {
	    	$("#edit-habit-error").html("Failed to find habit.");
	    	return;
	}

	habitRef = FB.habitsRef.child(params['hid']);

	habitRef.once('value', function (snap) {
		var form = document.forms['edit-habit'];

	    habit = snap.val();
	    if (!habit) {
	    	$("#edit-habit-error").html("Failed to find habit.");
	    	return;
	    }
	    
	    if (habit.icon == "uploadedIcon") {
	        var uploadedIcon = document.querySelector("#uploadedIcon");
	        uploadedIcon.src = habit.icon_src;
	        $(uploadedIcon).fadeIn();
	    }
        
        if (habit.times > 3) {
	    	$("#others").val(habit.times);
            $("#moreThan3").val(habit.times);     
	    	$(".hidden-if-inactive").show();
	    }
	    
	    form['title'].value = habit.title;
	    form['day'].value = habit.times;
	    form['icon'].value = habit.icon;	

	    var days = habit.dates;

	    var checkBoxes = form['date'];
	    for (checkBox in checkBoxes) {
	    	checkBoxes[checkBox].checked = days[checkBoxes[checkBox].value];
	    }

	    oldFreq = habit.times;
	});
});

function checkFrequency() {
    var other_times = document.getElementById("moreThan3")
    
    if (other_times.checked) {
        $("#moreThan3").val($("#others")[0].value);  
    }
} 

function getSearchParameters() {
      var prmstr = window.location.search.substr(1);
      return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
}

function transformToAssocArray( prmstr ) {
    var params = {};
    var prmarr = prmstr.split("&");
    for ( var i = 0; i < prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}

function previewFile() {
    var newIcon = document.querySelector('#uploadedIcon') //selects the query named img
           
    var file = document.querySelector('input[type=file]').files[0]; //sames as here
    reader  = new FileReader();

    reader.onloadend = function () {
        newIcon.src = reader.result;
        $(newIcon).fadeIn();
        customImgChanged = true;
    }

    if (file) {
        reader.readAsDataURL(file); //reads the data as a URL
    }
    
    newIcon.previousElementSibling.setAttribute("checked", "true");
}


function submitEditHabitForm() { 
    checkFrequency();
    
	var form = document.forms['edit-habit'];
	habit.title = form['title'].value;
	habit.times = parseInt(form['day'].value);
	if (habit.times != oldFreq) {
		habit.times_up_today = 0;
	    habit.times_down_today = 0;
	}
	habit.icon = form['icon'].value;
    habit.icon_src = form['icon'].value == "uploadedIcon" ? (customImgChanged ? reader.result : document.querySelector('#uploadedIcon').src) : '../img/' + form['icon'].value;
	habit.dates = {};

	var dates = form['date'];
	for (var i=0, len=dates.length; i<len; i++) {
    	habit.dates[dates[i].value] = dates[i].checked;
	}

	editHabit(habit);
	return false;
}

function editHabit(habitObj) {
    console.log(habitObj);

    var validation = FB.validateHabit(habitObj);
    if (validation) {
		$("#edit-habit-error").html(validation);
		return;
    }

	if (!habitRef) {
	    	$("#edit-habit-error").html("Failed to find habit.");
	    	return;
	}

    habitRef.update(habitObj, function onComplete(error) {
		if (error) {
			console.log('Synchronization failed');
		} else {
			console.log("Synchronization succeeded.");
			window.location.href = './list.html';
		}    	
    });
}