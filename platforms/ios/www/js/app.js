var FB = {}

FB.rootRef = new Firebase('https://glowing-fire-6794.firebaseio.com');
// Check the current user
FB.user = FB.rootRef.getAuth();

FB.checkLoggedIn = function() {
	if (!FB.user) {
		window.location.href = './login.html';
	}	
}

FB.checkLoggedIn();

FB.userRef = FB.rootRef.child('users').child(FB.user.uid);
FB.habitsRef = FB.userRef.child('habits');
FB.settingsRef = FB.userRef.child('settings');

FB.getHabitRef = function(id) {
	return FB.habitsRef.child(id);
}

FB.habitThumbsUp = function(id) {
	var habitRef = FB.getHabitRef(id);

    habitRef.once('value', function (snap) {
        var habit = snap.val();
        if (!habit) return;

        if (habit.times_down_today == 0)
        	habit.times_up_today++;

        if (habit.times_up_today == habit.times) {
            habit.streak++;
            if (habit.streak > habit.best_record) {
                habit.best_record = habit.streak;
            }
        }

        habitRef.update(habit, function onComplete(error) {
            if (error) {
                console.log('Synchronization failed');
            } else {
                console.log("Synchronization succeeded.");
            }
        });        
    });  
}

FB.habitThumbsDown = function(id) {
	var habitRef = FB.getHabitRef(id);

    habitRef.once('value', function (snap) {
        var habit = snap.val();
        if (!habit) return;

        habit.streak = 0;

        if (habit.times_down_today == 0)
        	habit.times_down_today++;

        habitRef.update(habit, function onComplete(error) {
            if (error) {
                console.log('Synchronization failed');
                $("#add-habit-error").html('Habit unsuccessfully added.');
            } else {
                console.log("Synchronization succeeded.");
            }
        });        
    });  
}

FB.habitDelete = function(id){
	FB.getHabitRef(id).remove();
};

FB.logout = function() {
	FB.rootRef.unauth();
	window.location.href = './login.html';
};

FB.validateHabit = function(habit) {
	if (habit.title == null || habit.title == "")
		return "Habit title must not be empty";

	if (!habit.title.match(/^[a-zA-Z0-9 _]+$/))
		return "Habit title must consist of only numbers and letters.";

	if (isNaN(habit.times) || habit.times < 0)
		return "Habit times must be a number larger than 0.";

	var date_count = 0;
	for (var date in habit.dates)
	{
		if (habit.dates[date])
			date_count++;
	}

	if (date_count < 1)
	{
		return "Must pick at least one weekday for your habit.";
	}

	if (!habit.times_up_today)
		habit.times_up_today = 0;

	if (!habit.times_down_today)
		habit.times_down_today = 0;

	if (!habit.streak)
		habit.streak = 0;

	if (!habit.best_record)
		habit.best_record = 0;

	return false;
};

FB.checkStatus = function(habit) {
	var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
	var today = days[new Date().getDay()];
	// Not today
	if (!habit.dates[today])
		return 0;

	// Failed
	if (habit.times_down_today > 0)
		return 2;

	// Complete
	if (habit.times_up_today >= habit.times)
		return 1;

	// In progress
	return 3;
}