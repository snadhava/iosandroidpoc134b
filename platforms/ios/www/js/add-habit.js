var reader;

function checkFrequency() {
    var other_times = document.getElementById("moreThan3")
    
    if (other_times.checked) {
        $("#moreThan3").val($("#others")[0].value);
    }
} 


function submitAddHabitForm() {
	var form = document.forms['add-habit'];
    
    // check if more than 3 radio button is pressed
    checkFrequency();
    
	var habitObj = {
		title: form['title'].value,
		times: parseInt(form['day'].value),
        icon: form['icon'].value,
        icon_src: form['icon'].value == "uploadedIcon" ? reader.result : '../img/' + form['icon'].value,
        streak: 0,
        best_record: 0,
        times_up_today: 0,
        times_down_today: 0
	};

	habitObj.dates = {};

	var dates = form['date'];
	for (var i=0, len=dates.length; i<len; i++) {
    	habitObj.dates[dates[i].value] = dates[i].checked;
	}

	addHabit(habitObj);
	return false;
}

function previewFile() {
    var newIcon = document.querySelector('#uploadedIcon') //selects the query named img
           
    var file = document.querySelector('input[type=file]').files[0]; //sames as here
    reader  = new FileReader();

    reader.onloadend = function () {
        newIcon.src = reader.result;
        $(newIcon).fadeIn();
    }

    if (file) {
        reader.readAsDataURL(file); //reads the data as a URL
    }
    
    newIcon.previousElementSibling.setAttribute("checked", "true");
  }


function addHabit(habitObj) {
    console.log(habitObj);

    var validation = FB.validateHabit(habitObj);
    if (validation) {
        $("#edit-habit-error").html(validation);
        return;
    }

    var newHabit = FB.habitsRef.push();
    newHabit.set(habitObj, function onComplete(error) {
		if (error) {
            $("#edit-habit-error").html("Failed to add habit.");
			console.log('Synchronization failed');
		} else {
			console.log("Synchronization succeeded.");
			window.location.href = './list.html';
		}    	
    });
} 


/*$(function() {
    $("input[name="day").click(function() 
    {
        if ( $("#age1").attr('checked'))
            $("#agetext").hide();
        if ( $("#age2").attr('checked'))
            $("#agetext").show();
    });
} */


/*
$(function(){
    
    var habitList = localStorage.getItem("habitList");//Retrieve the stored data
    var icon_img = "";
    var daily_freq = "";
    var weekly_freq = {};
    weekly_freq['sunday'] = false;
    weekly_freq['monday'] = false;
    weekly_freq['tuesday'] = false;
    weekly_freq['wednesday'] = false;
    weekly_freq['thursday'] = false;
    weekly_freq['friday'] = false;
    weekly_freq['saturday'] = false;

	habitList = JSON.parse(habitList); //Converts string to object
    
    if(habitList == null) //If there is no data, initialize an empty array
		habitList = [];
    
    function Add(){
		var client = JSON.stringify({
			Title    : $("#title").val(),
			Icon  : icon_img,
			WeeklyFrequency : weekly_freq,
			DailyFrequency : daily_freq,
            Streak: 0,
            BestRecord: 0
		});
		habitList.push(client);
		localStorage.setItem("habitList", JSON.stringify(habitList));
		alert("The data was saved.");
		return true;
	}
    
    $(".icon").bind("click", function() {
        icon_img = $(this).attr("src");
    });
    
    $("#habit-form").bind("submit", function(e) {
        e.preventDefault();
        console.log($(this).serialize());
        $("#ck-button").find("input").each(function() {
            var day = "";
            if($(this).is(':checked')) {
                day = $(this).attr("value");
                weekly_freq[day] = true;
            }
        });
        
        daily_freq = $("#daily-button input[name=day]:checked").val();
        
        Add();
        window.location.href = './list.html';
    });
});*/
